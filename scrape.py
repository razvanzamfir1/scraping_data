import requests  # ne lasa sa download-am HTML-ul
from bs4 import BeautifulSoup # ne lasa sa uitilizam HTML-ul si sa scoatem anumite date si sa facem ce vrem cu ele
import pprint # se importa acest modul care la apelul functiei arajeaza mai frumos in terminal titlu, voturile, si scorul

res = requests.get('https://news.ycombinator.com/news')  # requestul este ca un web browser
res2 = requests.get('https://news.ycombinator.com/news?p=2')  # requestul este ca un web browser
  #!!! trebuie intrat in Crome la more tools -> developer tools -> Network(refresh la pagina) -> deschis news pt a vedea fisierul HTML
                 # -> apoi revenim in consola si dam comanda python scrape.py pentru a lua informatia de pe website. sa nu uitam sa punem la print res.text.(text este textul nostru pe care il luam)
soup = BeautifulSoup(res.text, 'html.parser') # aici converim cu Beautuful soup din string in HTML informatia pentru a o putea utiliza. parser = analizator
soup2 = BeautifulSoup(res.text, 'html.parser')

# Exemple
# print(soup.body) # pentru a afisa doar body-ul
# print(soup.body.contents)   # pentru a afisa body; .content va afisa contentul din body intr-o lista []
# print (soup.find_all('div')) # ne afiseaza toate div-urile pe care le contine pagina
# print (soup.find_all('a')) # ne afiseaza toate link-urile pe care le contine pagina
# print (soup.find('a')) # ne afiseaza primul a gasit
# print (soup.find('title')) # ne afiseaza titul gasit
# print(soup.find(id='33518443'))  # ne scoate o anumita informatie dupa numarul id-ului, de exemplu un tag

# este recomnadat sa folosim select() pentru a selecta date din soup pe care l-am creat si downloud-dat, utilizand CSS selctor

# Exemple
# print(soup.select('a'))  # selecteaza toate tag-urile a
# print(soup.select('.spacer'))  # selecteaza toate clasele la fel din pagina intr-o lista, ca la CSS (.clasa)
# print(soup.select('#pagespace'))  # selecteaza toate id-urile la fel din pagina intr-o lista, ca la CSS (#id)

links = soup.select('.titleline > a')
subtext = soup.select('.subtext')

# # Pentru a naviga prin mai multe pagini trebuie sa duplicam ceea ce am facut anterior sa unim pe cele anterioare cu duplicatele modif2 intr-o variabila noua si sa scimbam apelul

links2 = soup2.select('.titleline > a')
subtext2 = soup2.select('.subtext')
#
mega_links = links +links2
mega_subtext = subtext + subtext2
# # print(votes[0])
# # print(votes[0].get('id'))
#
def sort_stories_by_votes(hnlist):
    return sorted(hnlist, key=lambda k: k['votes'], reverse=True)  # aici sortam in ordine descrescatoare dupa cheia dictionarului votes

def create_custom_hn(links, subtext):
    hn = []
    for idx, item in enumerate(links):
        title = item.getText()
        href = item.get('href', None)
        vote = subtext[idx].select('.score')
        if len(vote):  # daca exista voturi; sunt unele articole care nu au voturi
            points = int(vote[0].getText().replace(' points', ''))  # aici ne va scoate numerele fara spatiul si points dupa, doar numerele
            if points > 99:
                hn.append({'title': title, 'link': href, 'votes': points})
    return sort_stories_by_votes(hn)

pprint.pprint(create_custom_hn(mega_links, mega_subtext))

# robot.text pentru a vedea ce permite site-ul sa faci
# alt tool in afara de beautifulsoap este Scrapy, este un framework care se foloseste pentru multe date site-uri, etc

